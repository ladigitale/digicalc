# Digicalc

Digicalc est une interface graphique simple pour créer des feuilles de calcul sur un serveur Ethercalc (https://github.com/audreyt/ethercalc).

Elle est publiée sous licence GNU AGPLv3.
Sauf la fonte HKGrotesk (Sil Open Font Licence 1.1)

### Variables d'environnement (fichier .env à créer à la racine du dossier)
```
AUTHORIZED_DOMAINS (* ou liste des domaines autorisés pour les requêtes POST et l'API, séparés par une virgule)
ETHERCALC_SERVER (lien vers un serveur Ethercalc)
```

### Serveur PHP nécessaire pour l'API
```
php -S localhost:8000 (pour le développement uniquement)
```

### Démo
https://ladigitale.dev/digicalc/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

