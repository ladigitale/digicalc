﻿<?php

session_start();

$env = '.env';
$donneesEnv = '';
if (isset($_SESSION['serveurEthercalc']) && $_SESSION['serveurEthercalc'] !== '') {
	$ethercalc_server = $_SESSION['serveurEthercalc'];
} else if (file_exists($env)) {
	$donneesEnv = explode("\n", file_get_contents($env));
	foreach ($donneesEnv as $ligne) {
		preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
		if (isset($matches[2])) {
			putenv(trim($ligne));
		}
	}
	$ethercalc_server = getenv('ETHERCALC_SERVER');
	$_SESSION['serveurEthercalc'] = $ethercalc_server;
}

if (isset($_SESSION['domainesAutorises']) && $_SESSION['domainesAutorises'] !== '') {
	$domainesAutorises = $_SESSION['domainesAutorises'];
} else if ($donneesEnv !== '') {
	$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
	$_SESSION['domainesAutorises'] = $domainesAutorises;
} else if (file_exists($env)) {
	$donneesEnv = explode("\n", file_get_contents($env));
	foreach ($donneesEnv as $ligne) {
		preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
		if (isset($matches[2])) {
			putenv(trim($ligne));
		}
	}
	$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
	$_SESSION['domainesAutorises'] = $domainesAutorises;
} else {
	echo 'erreur';
	exit();
}

if ($domainesAutorises === '*') {
	$origine = $domainesAutorises;
} else {
	$domainesAutorises = explode(',', $domainesAutorises);
	$origine = $_SERVER['SERVER_NAME'];
}
if ($origine === '*' || in_array($origine, $domainesAutorises, true)) {
	header('Access-Control-Allow-Origin: $origine');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Max-Age: 1000');
	header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
} else {
	echo 'erreur';
	exit();
}

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST['token']) && !empty($_POST['lien'])) {
	$token = $_POST['token'];
	$domaine = $_SERVER['SERVER_NAME'];
	$lien = $_POST['lien'];
	$donnees = array(
		'token' => $token,
		'domaine' => $domaine
	);
	$donnees = http_build_query($donnees);
	$ch = curl_init($lien);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $donnees);
	$resultat = curl_exec($ch);
	if ($resultat === 'non_autorise' || $resultat === 'erreur') {
		echo 'erreur_token';
	} else if ($resultat === 'token_autorise' && !empty($_POST['action'])) {
		$action = $_POST['action'];
		if ($action === 'creer' && !empty($_POST['slug'])) {
			$slug = $_POST['slug'];
			$url = $ethercalc_server . '/_';
			$data = array('room' => $slug, 'snapshot' => $slug);
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0');
			$reponse = curl_exec($curl);
			if (curl_errno($curl)) {
				$reponse = 'erreur';
			}
			curl_close($curl);
			if ($reponse !== 'erreur') {
				echo'contenu_cree';
			} else {
				echo $reponse;
			}
		} else if ($action === 'supprimer' && !empty($_POST['id'])) {
			$id = $_POST['id'];
			$url = $ethercalc_server . '/_/' . $id;
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
			$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			$reponse = curl_exec($curl);
			if (curl_errno($curl)) {
				$reponse = 'erreur';
			}
			if ($reponse === 201) {
				echo 'contenu_supprime';
			} else {
				echo $reponse;
			}
			curl_close($curl);
		} else {
			echo 'erreur';
		}
		exit();
	} else {
		echo 'erreur';
		exit();
	}
	curl_close($ch);
} else {
	echo 'erreur';
	exit();
}

?>
