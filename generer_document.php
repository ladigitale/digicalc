﻿<?php

session_start();

$env = '.env';
$donneesEnv = '';
if (isset($_SESSION['serveurEthercalc']) && $_SESSION['serveurEthercalc'] !== '') {
	$ethercalc_server = $_SESSION['serveurEthercalc'];
} else if (file_exists($env)) {
	$donneesEnv = explode("\n", file_get_contents($env));
	foreach ($donneesEnv as $ligne) {
		preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
		if (isset($matches[2])) {
			putenv(trim($ligne));
		}
	}
	$ethercalc_server = getenv('ETHERCALC_SERVER');
	$_SESSION['serveurEthercalc'] = $ethercalc_server;
} else {
	$ethercalc_server = '';
}

if (isset($_SESSION['domainesAutorises']) && $_SESSION['domainesAutorises'] !== '') {
	$domainesAutorises = $_SESSION['domainesAutorises'];
} else if ($donneesEnv !== '') {
	$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
	$_SESSION['domainesAutorises'] = $domainesAutorises;
} else if (file_exists($env)) {
	$donneesEnv = explode("\n", file_get_contents($env));
	foreach ($donneesEnv as $ligne) {
		preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
		if (isset($matches[2])) {
			putenv(trim($ligne));
		}
	}
	$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
	$_SESSION['domainesAutorises'] = $domainesAutorises;
} else {
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Max-Age: 1000');
	header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
	$domainesAutorises = '';
}

if ($domainesAutorises !== '') {
	if ($domainesAutorises === '*') {
		$origine = $domainesAutorises;
	} else {
		$domainesAutorises = explode(',', $domainesAutorises);
		$origine = $_SERVER['SERVER_NAME'];
	}
	if ($origine === '*' || in_array($origine, $domainesAutorises, true)) {
		header('Access-Control-Allow-Origin: $origine');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
	} else {
		header('Location: /');
		exit();
	}
}

if (!empty($_POST['creation'])) {
	$id = uniqid('', false);
	$url = $ethercalc_server . '/_';
	$data = array('room' => $id, 'snapshot' => $id);
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0');
	$reponse = curl_exec($curl);
	if (curl_errno($curl)) {
		$reponse = 'erreur';
	}
	curl_close($curl);
	if ($reponse !== 'erreur') {
		echo $ethercalc_server . '/' . $id;
	} else {
		echo $reponse;
	}
	exit();
} else {
	header('Location: /');
}

?>
